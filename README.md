Research and Engineering for NWSC

The aim of the project was to allow users/customers of NWSC to be able to report to the team at NWSC, issues and complaints regarding water supply and distribution problems in their areas. This was a joint project with friends of mine (TEAM DEWS), at the Sanitation Hackathon event that took place at CIT, Makerere, 2013.

Anyone interested in improving this project, please fork or contribute or merely clone as use.
