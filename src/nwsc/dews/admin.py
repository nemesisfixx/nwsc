from django.contrib import admin
from django.contrib.sites.management import Site
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from dews.models import *
from django.contrib import messages


#----- logging
import logging
from nwsc.settings import MAIN_LOG, URL_PREFIX,LOG_LEVEL,STATIC_URL

logging.basicConfig(level=LOG_LEVEL, filename=MAIN_LOG)
logger = logging.getLogger(__name__)


admin.site.unregister( Site )

class RegionAdmin(admin.ModelAdmin):
    pass

admin.site.register( Region, RegionAdmin)


class ClientAdmin(admin.ModelAdmin):
    pass

admin.site.register( Client, ClientAdmin)

class ReporterAdmin(admin.ModelAdmin):
    pass

admin.site.register( Reporter, ReporterAdmin)


class IssueCategoryAdmin(admin.ModelAdmin):
    pass

admin.site.register( IssueCategory, IssueCategoryAdmin)


class TrackingStatusAdmin(admin.ModelAdmin):
    pass

admin.site.register( TrackingStatus, TrackingStatusAdmin)


class IssueAdmin(admin.ModelAdmin):
    pass

admin.site.register( Issue, IssueAdmin)


class DepartmentAdmin(admin.ModelAdmin):
    pass

admin.site.register( Department, DepartmentAdmin)


class StaffAdmin(admin.ModelAdmin):
    pass

admin.site.register( Staff, StaffAdmin)
