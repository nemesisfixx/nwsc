from django.db import models
from colorful.fields import RGBColorField


class Region(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    name = models.CharField( max_length = 50 )
    def __unicode__(self):
        return "Region : %s" % ( self.name )
    class Meta:
        verbose_name = 'Region'
        verbose_name_plural = 'Regions'

class Client(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
    account_number = models.TextField()
    name = models.CharField( max_length = 100 )
    phone = models.CharField( max_length = 20 )
    email = models.CharField( max_length = 100, null = True, blank = True )
    region = models.ForeignKey( Region, related_name = 'clients', null = True, blank = True )
    def __unicode__(self):
        return "%s : %s" % ( self.account_number, self.name)
    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'


class Reporter(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
    name = models.CharField( max_length = 100 )
    phone = models.CharField( max_length = 20 )
    email = models.CharField( max_length = 100, null = True, blank = True )
    client_account = models.ForeignKey( Client, related_name = 'reporter_account', null = True, blank = True )
    def __unicode__(self):
        return "Reporter : %s%s" % (self.name , ' (%s)' % self.client_account.account_number if self.client_account is not None else '')
    class Meta:
        verbose_name = 'Reporter'
        verbose_name_plural = 'Reporters'

class IssueCategory(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    name = models.CharField( max_length = 50 )
    def __unicode__(self):
        return "%s" % (self.name)
    class Meta:
        verbose_name = 'Issue Category'
        verbose_name_plural = 'Issue Categories'


class TrackingStatus(models.Model):
    TRACKING_STATUSES = (
            ('created','CREATED'),
            ('assigned','ASSIGNED'),
            ('active','ACTIVE'),
            ('stalled','STALLED'),
            ('closed','CLOSED'),
            )
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
    name = models.CharField(max_length=50,choices=TRACKING_STATUSES)
    color = RGBColorField(verbose_name='Representative Color')
    is_initial = models.NullBooleanField('Is this the INITIAL Status?', help_text='If this is the INITIAL Status, then the system can automatically select it when a new issue is created with not particular status assigned', default=False,blank=True,null=True, unique = True)
    is_assigned = models.NullBooleanField('Is this the ASSIGNMENT Status?', help_text='If this is the ASSIGNMENT Status, then the system can automatically select it when a new issue is assigned to someone', default=False,blank=True,null=True, unique = True)
    is_stalled = models.NullBooleanField('Is this the STALLED Status?',help_text='If this is the STALLED Status, the System can automatically pick it when it detects that an issue has stalled', default=False,blank=True,null=True, unique = True)
    is_active = models.NullBooleanField('Is this the ACTIVE Status?',help_text='If this is the ACTIVE Status, the System can automatically pick it when it detects that there\'s activity concerning an issue', default=False,blank=True,null=True, unique = True)
    is_final = models.NullBooleanField('Is this the FINAL Status?',help_text='If this is the FINAL Status, then the system will assign it to the issue once it detects that the issue is closed / resolved', default=False,blank=True,null=True, unique = True)
    def view_color(self):
        return '<div style="background-color:%s;display:block;width:40;height:25px;padding:2px;"></div>' % self.color
    view_color.allow_tags = True
    view_color.short_description = 'Representative Color'
    @staticmethod
    def get_initial():
        try:
            return TrackingStatus.objects.filter(is_initial=True)[0]
        except:
            return None
    @staticmethod
    def get_stalled():
        try:
            return TrackingStatus.objects.filter(is_stalled=True)[0]
        except:
            return None
    @staticmethod
    def get_assigned():
        try:
            return TrackingStatus.objects.filter(is_assigned=True)[0]
        except:
            return None
    @staticmethod
    def get_final():
        try:
            return TrackingStatus.objects.filter(is_final=True)[0]
        except:
            return None
    @staticmethod
    def get_active():
        try:
            return TrackingStatus.objects.filter(is_active=True)[0]
        except:
            return None
    def __unicode__(self):
        return "%s" % (self.get_name_display())
    class Meta:
        verbose_name = 'Tracking Status'
        verbose_name_plural = 'Tracking Statuses'

class Issue(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
    category = models.ForeignKey( IssueCategory, related_name = 'issues' )
    region = models.ForeignKey( Region, related_name = 'issues' )
    latitude = models.FloatField( null = True, blank = True )
    longitude = models.FloatField( null = True, blank = True )
    comment = models.TextField( null = True, blank = True )
    reporter = models.ForeignKey( Reporter, related_name = 'issues', null = True, blank = True )
    affected_client = models.ForeignKey( Client, related_name = 'issues', null = True, blank = True )
    status = models.ForeignKey( TrackingStatus, related_name = 'issues' )
    def comment_summary(self, limit = 10):
        if self.comment:
            return self.comment[:limit] + ('...' if len(self.comment) > limit else '')
        else:
            return 'No Comment'
    def __unicode__(self):
        return "%s : %s : %s : %s" % (self.region.name, self.category.name, self.status, self.comment_summary())
    class Meta:
        verbose_name = 'Issue'
        verbose_name_plural = 'Issues'

class Department(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
    name = models.CharField( max_length = 100 )
    def __unicode__(self):
        return "Department : %s" % (self.name)
    class Meta:
        verbose_name = 'Staff Department'
        verbose_name_plural = 'Staff Departments'

class Staff(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
    department = models.ForeignKey( Department, related_name = 'staff')
    name = models.CharField( max_length = 100 )
    phone = models.CharField( max_length = 20 )
    email = models.CharField( max_length = 100 )
    assigned_regions = models.ManyToManyField( Region, related_name = 'staff' )
    assigned_categories = models.ManyToManyField( IssueCategory, related_name = 'staff' )
    assigned_clients = models.ManyToManyField( Client, related_name = 'staff', null = True, blank = True )
    def __unicode__(self):
        return "%s" % (self.department.name, self.name)
    class Meta:
        verbose_name = 'Staff Member'
        verbose_name_plural = 'Staff'
