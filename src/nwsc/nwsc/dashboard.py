from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name

from nwsc.settings import URL_PREFIX

class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        # append a group for "Administration" & "Applications"
        self.children.append(modules.Group(
            _('Administration'),
            column=1,
            collapsible=True,
            children = [
                modules.AppList(
                    _('System Administration'),
                    column=1,
                    collapsible=False,
                    models=(
                        'django.contrib.*',
                        ),
                ),
                modules.ModelList(
                    _('Staff Management'),
                    column=1,
                    css_classes=('collapse closed',),
                    models=(
                        'dews.models.Department',
                        'dews.models.Staff',
                        ),
                ),
                modules.ModelList(
                    _('Client Management'),
                    column=1,
                    css_classes=('collapse closed',),
                    models=(
                        'dews.models.Client',
                        'dews.models.Reporter',
                        ),
                )

            ]
        ))

        self.children.append(modules.ModelList(
            _('Issue Tracking'),
            column=2,
            collapsible=False,
            models=(
                'dews.models.Issue',
                'dews.models.IssueCategory',
                'dews.models.Region',
                'dews.models.TrackingStatus',
                )
        ))

        '''
        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('IMPORTANT MODULES'),
            column=2,
            children=[
                {
                    'title': _('Parcel Activity Dashboard'),
                    'url': '%s/admin/dashboard/parcels' % URL_PREFIX,
                    'external': False,
                },
                {
                    'title': _('Consignment Activity Dashboard'),
                    'url': '%s/admin/dashboard/consignments' % URL_PREFIX,
                    'external': False,
                },
                {
                    'title': _('System Settings'),
                    'url': '%s/admin/dews/systemsettings' % URL_PREFIX,
                    'external': False,
                },
            ]
        ))
        '''

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))


