from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to
from django.contrib import admin
from nwsc.settings import URL_PREFIX
admin.autodiscover()

urlpatterns = patterns('',
    ( r'^grappelli/', include('grappelli.urls') ),
     url(r'^$',redirect_to,{'url':'admin'}),
     url(r'^admin/', include(admin.site.urls)),
)

'''
urlpatterns = patterns('nwsc.dews.views',
     (r'^api/track_status_list/?','track_status_list'),
     (r'^api/mobile/login/?','auth_mob_client'),
     (r'^api/mobile/trackpoint/?','track_point'),
     (r'^api/mobile/deliveryproof/?','delivery_proof'),
     (r'^map/?$', 'map'),
     (r'^pdf/deliveryproof/(?P<id>\d+)/?$', 'pdf_deliveryproof'),
     (r'^pdf/asset/(?P<id>\d+)/?$', 'pdf_asset'),
     (r'^pdf/transaction/(?P<id>\d+)/?$', 'pdf_transaction'),
     (r'^pdf/manifest/(?P<id>\d+)/?$', 'pdf_manifest'),
     (r'^qrcode/?$', 'qrcode'),
     (r'^apply/?$', 'client_apply'),
     (r'^admin/dashboard/parcels/?$', 'dashboard_parcels'),
     (r'^admin/dashboard/consignments/?$', 'dashboard_consignments'),
     (r'^admin/dews/assetgroup/autoadd/(?P<id>\d+)/?$', 'auto_add_assets'),
     )
'''



